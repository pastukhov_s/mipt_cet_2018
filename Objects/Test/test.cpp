#include "pch.h"
#include "../Objects/complex.h"
#include "../Objects/string.h"

TEST(ComplexNumbersTest, Adding)
{
	myspc::Complex a, b, c;

	a.SetReal(0);
	a.SetImg(0);
	b.SetReal(1);
	b.SetImg(1);
	c.SetReal(-1);
	c.SetImg(-1);

	ASSERT_EQ(a, b + c);
}

TEST(ComplexNumbersTest, Substracting)
{
	myspc::Complex a, b, c;

	a.SetReal(2);
	a.SetImg(2);
	b.SetReal(1);
	b.SetImg(1);
	c.SetReal(-1);
	c.SetImg(-1);

	ASSERT_EQ(a, b - c);
}

TEST(ComplexNumbersTest, Multiplicating)
{
	myspc::Complex a, b, c;

	a.SetReal(0);
	a.SetImg(-2);
	b.SetReal(1);
	b.SetImg(1);
	c.SetReal(-1);
	c.SetImg(-1);

	ASSERT_EQ(a, b * c);
}

TEST(ComplexNumbersTest, Dividing)
{
	myspc::Complex a, b, c;

	a.SetReal(-1);
	a.SetImg(0);
	b.SetReal(1);
	b.SetImg(1);
	c.SetReal(-1);
	c.SetImg(-1);

	ASSERT_EQ(a, b / c);
}

TEST(ComplexNumbersTest, Conjugate)
{
	myspc::Complex a, b;

	a.SetReal(-1);
	a.SetImg(-1);
	b.SetReal(-1);
	b.SetImg(1);

	ASSERT_EQ(a, ~b);
}


TEST(ComplexNumbersTest, Modulus)
{
	myspc::Complex a;

	a.SetReal(3);
	a.SetImg(4);

	ASSERT_EQ(5, abs(a));
}


TEST(StringTest, ComparingStrings)
{
	myspc::String s1, s2;
	s1 = myspc::String("qwerty");
	s2 = myspc::String("rty");

	ASSERT_NE(s1, s2);
}


TEST(StringTest, IsEmpty)
{
	myspc::String s1;
	s1 = myspc::String("");

	ASSERT_EQ(s1, "");
}

TEST(StringTest, CharLoc)
{
	myspc::String s1;
	s1 = myspc::String("123123");
	std::cout << (s1[0]) << std::endl;

	ASSERT_EQ(s1[0], '1');
}

