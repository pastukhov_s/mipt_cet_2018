#pragma once

#pragma warning(disable : 4996)

#ifndef STRING
#define STRING

namespace myspc {

	class String {
	public:

		String();
		String(const char *c);
		String(const String& t);
		~String();

		String& operator=(const String& t);
		String operator+(const String& t) const;
		String& operator=(const char *c);
		String& operator+=(const String& t);
		char& operator[](size_t i);
		const char& operator[](size_t i) const;

		char& at(size_t i);
		void get_input(std::istream& in);
		size_t get_length() const;
		void from_c_str(const char *other);

		friend std::ostream& operator<<(std::ostream& out, const String& t);
		friend std::istream& operator>>(std::istream& in, String& t);
		friend bool operator==(const String& t, char *c);
		friend bool operator==(const String& t, const String& t1);
		friend bool operator!=(const String& t, char *c);
		friend bool operator!=(const String& t, const String& t1);

	private:

		char *str;
		size_t length;
	};

	String::String()
	{
		length = 1;
		str = '\0';
	}

	String::String(const char *c)
	{
		from_c_str(c);
	}

	String::String(const String& t)
	{
		from_c_str(t.str);
	}

	String::~String()
	{
		delete[] str;
	}

	String& String::operator=(const String& t)
	{
		from_c_str(t.str);

		return *this;
	}

	String& String::operator=(const char *c)
	{
		from_c_str(c);

		return *this;
	}

	String String::operator+(const String& t) const
	{
		return (String(str) += t.str);
	}

	bool operator==(const String& t, char *c)
	{
		return (strcmp(t.str, c) == 0);
	}

	bool operator==(const String& t, const String& t1)
	{
		return (strcmp(t.str, t1.str) == 0);
	}

	bool operator!=(const String& t, char *c)
	{
		return !(operator==(t, c));
	}

	bool operator!=(const String& t, const String& t1)
	{
		return !(operator==(t, t1.str));
	}

	String& String::operator+=(const String& t)
	{
		length = strlen(str) + strlen(t.str) + 1;
		char *newStr = new char[length];
		std::strcpy(newStr, str);
		std::strcat(newStr, t.str);
		std::strcpy(str, newStr);
		str[length - 1] = '\0';
		delete[] newStr;

		return *this;
	}

	char& String::operator[](size_t i)
	{
		return str[i];
	}

	const char& String::operator[](size_t i) const
	{
		return str[i];
	}

	char& String::at(size_t i)
	{
		if (str != NULL && i >= 0 && i < length)
		{
			return str[i];
		}
		else
		{
			throw std::exception("wrong index");
		}
	}

	void String::get_input(std::istream& in)
	{
		while (true)
		{
			char c = in.get();

			if (c == '\n')
			{
				break;
			}

			if (length == 0)
			{
				length = 2;
			}
			else
			{
				++length;
			}

			char* newStr = new char[length];
			if (str != NULL)
			{
				std::strcpy(newStr, str);
			}
			newStr[length - 2] = c;
			newStr[length - 1] = '\0';

			delete[] str;

			str = newStr;
		}
	}

	size_t String::get_length() const
	{
		return (length - 1);
	}

	void String::from_c_str(const char *c)
	{
		length = strlen(c) + 1;
		str = new char[length];
		std::strcpy(str, c);
	}

	std::ostream& operator<< (std::ostream& out, const String& t)
	{
		out << t.str;
		return out;
	}

	std::istream& operator>> (std::istream& in, String& t)
	{
		t.get_input(in);
		return in;
	}

}
#endif