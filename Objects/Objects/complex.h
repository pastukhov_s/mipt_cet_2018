#pragma once

#ifndef COMPLEX
#define COMPLEX

namespace myspc {

	class Complex
	{
	public:
		Complex(const double r_ = 0, const double i_ = 0);

		double GetImg()const;
		double GetReal()const;

		void SetImg(const double);
		void SetReal(const double);

	private:
		double m_real;
		double m_img;
	};

	double abs(const Complex a_)
	{
		double a = a_.GetReal(), b = a_.GetImg();
		double value(sqrt(a*a + b * b));
		return value;
	}

	//***************************************
	// Overloading operators +, -, /, *, == *
	// Adding operator ~ for conjugation    *
	//***************************************
	
	Complex operator*(const Complex a_, const Complex b_)
	{
		double a = a_.GetReal(), b = a_.GetImg(), c = b_.GetReal(), d = b_.GetImg();
		Complex value(a*c - b * d, a*d + b * c);
		return value;
	}

	Complex operator-(const Complex a_, const Complex b_)
	{
		double a = a_.GetReal(), b = a_.GetImg(), c = b_.GetReal(), d = b_.GetImg();
		Complex value(a - c, b - d);
		return value;
	}

	Complex operator+(const Complex a_, const Complex b_)
	{
		double a = a_.GetReal(), b = a_.GetImg(), c = b_.GetReal(), d = b_.GetImg();
		Complex value(a + c, d + b);
		return value;
	}

	Complex operator~(const Complex a_)
	{
		double a = a_.GetReal(), b = a_.GetImg();
		Complex value(a, -b);
		return value;
	}

	Complex operator/(const Complex a_, const Complex b_)
	{
		double a = a_.GetReal(), b = a_.GetImg(), c = b_.GetReal(), d = b_.GetImg();
		Complex value((a*c + b * d) / (c*c + d * d), (b*c - a * d) / (c*c + d * d));
		return value;
	}

	bool operator==(const Complex a_, const Complex b_)
	{
		return (a_.GetReal() == b_.GetReal() && a_.GetImg() == b_.GetImg());
	}

	std::ostream& operator<<(std::ostream& os_, const Complex& comp_)
	{
		return os_ << '(' << comp_.GetReal() << ',' << comp_.GetImg() << "i)";
	}

	Complex::Complex(const double r_, const double i_) : m_real(r_), m_img(i_)
	{}
	
	double Complex::GetReal()const
	{
		return m_real;
	}

	double Complex::GetImg()const
	{
		return m_img;
	}

	void Complex::SetReal(const double r_)
	{
		m_real = r_;
	}

	void Complex::SetImg(const double i_)
	{
		m_img = i_;
	}
	
}
#endif