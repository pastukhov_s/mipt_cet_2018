// complex_numders.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include "pch.h"
#include <iostream>
#include "complex.h"
#include "string.h"

int main() {

	std::string s1 = "";
	s1 += "1";
	//s1 = std::string("123123");
	std::cout << s1 << std::endl;

	// declaration of two complex numbers to be operated at and one variable for calculations
	myspc::Complex c1, c2, z;
	// declaration of two real parts and two imaginary parts and one variable for complex modulus
	double x1, y1, x2, y2, z_abs;

	// data input - calculations - output
	std::cout << "enter real part of the first complex number: " << std::endl;
	std::cin >> x1;
	c1.SetReal(x1);
	std::cout << "enter imaginary part of the first complex number: " << std::endl;
	std::cin >> y1;
	c1.SetImg(y1);

	std::cout << "first complex number: " << std::endl
			  << c1 << std::endl
			  << "and it's conjugation: " << std::endl;
	z = ~c1;
	std::cout << z << std::endl
			  << "enter real part of the second complex number: " << std::endl;
	std::cin >> x2;
	c2.SetReal(x2);
	std::cout << "enter imaginary part of the second complex number: " << std::endl;
	std::cin >> y2;
	c2.SetImg(y2);
	std::cout << "second complex number: " << std::endl
			  << c1 << std::endl
			  << "and it's conjugation: " << std::endl;
	z = ~c2;
	std::cout << z << std::endl
			  << "the sum of two complex numbers: " << std::endl;
	z = c1 + c2;
	std::cout << z << std::endl
			  << "the difference of two complex numbers: " << std::endl;
	z = c1 - c2;
	std::cout << z << std::endl
			  << "the multiplication of two complex numbers: " << std::endl;
	z = c1 * c2;
	std::cout << z << std::endl
			  << "the division of two complex numbers: " << std::endl;
	z = c1 / c2;
	std::cout << z << std::endl
			  << "the modulus of the first complex number: " << std::endl;
	z_abs = abs(c1);
	std::cout << z_abs << std::endl
			  << "the modulus of the second complex number: " << std::endl;
	z_abs = abs(c2);
	std::cout << z_abs << std::endl;
	return 0;
}